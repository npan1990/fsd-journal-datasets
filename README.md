# A General Framework for First Story Detection Utilizing Entities and their Relations #

In this repository you will be  able to find supplementary material regading the paper. More specifically we information about:

* A subset of the  datasets that are not copyright protected
* The link to the first story annotation tool repository
* The links to other resources used
* The link for the VM with the notebooks of the paper

### Event Registry Datasets ###

* ER-Business: 2019 documents, 303 events
* ER-Society: 2037 documents , 134 events

Contact the first author for access to the datasets

### Google News Datasets ###

* XML provided by the authors: http://www.db-net.aueb.gr/GoogleNewsDataset/

**IMPORTANT:** Please cite the paper "Karkali, M., Rousseau, F., Ntoulas, A., & Vazirgiannis, M. (2013, October). Efficient online novelty detection in news streams. In International conference on web information systems engineering (pp. 57-71). Springer, Berlin, Heidelberg." if  you use the Google-News dataset. 

### Scale News Aggregator Daataset ### 

* https://archive.ics.uci.edu/ml/datasets/News+Aggregator

**IMPORTANT** : Please cite the paper: "Fabio Gasparetti. 2017. Modeling user interests from web browsing activities. Data Min. Knowl. Discov. 31, 2 (March 2017), 502-547. DOI"

### Ready to use Datasets

* http://195.134.67.98/Datasets/tkde2020/

### Annotator ###

* Repository:  https://bitbucket.org/npan1990/firststory-annotator

### Virtual Machine ###

* VBox Image: Ubuntu server with jupyter notebooks (~20GB), VirtualBox virtual disk image.

Contact the first author for access to the VM. 
Password: j0urn@!

### Who do I talk to? ###

* Nikolaos Panagiotou (npanagio <AT> di.uoa.gr)